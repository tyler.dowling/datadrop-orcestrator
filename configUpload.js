const { publish } = require("./pubsub-service");

exports.upload = async (req, res) => {
    if (req.method != "POST") {
        console.error("Bad request found:", req);
        res.status(400);
        res.send("Bad input, not a post request");
    }
    if(!req.body.services) {
        console.error("Bad request found:", req);
        res.status(400);
        res.send("Bad input, services could not be found");
    }
    await publish(req.body);
    res.status(201).end();
};