const { PubSub } = require('@google-cloud/pubsub');
const pubSubClient = new PubSub();

const ftpPubSubTopic = pubSubClient.topic(process.env.PUB_SUB_TOPIC_FTP,{
    batching: {
        maxMessages: 10,
        maxMilliseconds: 1000,
    },
});

const psPubSubTopic = pubSubClient.topic(process.env.PUB_SUB_TOPIC_PS_VPN,{
    batching: {
        maxMessages: 10,
        maxMilliseconds: 1000,
    },
});


exports.publish = async (config) => {
    const publishes = [];
    for(const service of config.services) {
        for (const feed of service.feeds) {
            if (feed.custom && feed.custom.scp) {
                try {
                    publishes.push(ftpPubSubTopic.publish(Buffer.from(JSON.stringify(config))));
                } catch (error) {
                    console.error(`Received error while publishing to ftp/scp pubsub: ${error.message}`);
                }
            }
            if (feed.custom && feed.custom.powerschool) {
                try {
                   publishes.push(psPubSubTopic.publish(Buffer.from(JSON.stringify(config))));
                } catch (error) {
                    console.error(`Received error while publishing to powerschool pubsub: ${error.message}`);
                }
            }
        }
    }
    await Promise.all(publishes);
}