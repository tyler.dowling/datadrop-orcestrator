const { MongoClient } = require('mongodb')
const { publish } = require("./pubsub-service");

exports.run = async (req, res) => {
    if (req.method != "POST") {
        console.error("Bad request found:", req);
        res.status(400);
        res.send("Bad input, not a post request");
    }

    // Should be URL with u/pw from google secret
    const client = new MongoClient(process.env.MONGO_HOST_URL);
    await client.connect();
    const db = client.db(process.env.MONGO_DB_NAME);
    // Collection for all the configs
    const collection = db.collection(process.env.MONGO_CONFIG_COLLECTION);

    const filteredDocs = await collection.find({"services.feeds" :{$elemMatch :     {$or: [
                    { "custom.scp" : { $exists: true, $ne: null }},
                    { "custom.powerschool" : { $exists: true, $ne: null }}
                ]
            }}}).toArray();


    if(filteredDocs){
        const publishes = [];
            for(const config of filteredDocs) {
                publishes.push(publish(config))
            }
        await Promise.all(publishes);
    }

    // Get's here and no errors so we cool
    res.status(201).end();
};