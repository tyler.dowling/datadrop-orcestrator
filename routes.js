const express = require("express");
const router = express.Router();
const runner = require("./runner");
const configUpload = require("./configUpload");


let routes = (app) => {
    // Running individual configs to PULL data
    router.post("/config/upload", configUpload.upload);

    // Polling mongo to run required integrations the PULL data
    router.post("/run", runner.run);


    app.use(router);
};

module.exports = routes;

