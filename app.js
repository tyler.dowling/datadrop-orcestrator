const express = require("express");
const app = express();

global.__basedir = __dirname;
const initRoutes = require("./routes");

app.use(express.urlencoded({ extended: true }));
initRoutes(app);

module.exports = {
    app
};